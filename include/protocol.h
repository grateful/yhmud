
#ifndef __ZJMUD__
#define __ZJMUD__

#define WZKEY		"ZWUxMTIyMDAwMw=="

#define ZJPAYPORT		3001

#define ESA		""
//字串分隔符，比如向客户端发送场景中的多个npc信息，npc信息之间可用此分隔符连接
#define WZSEP		"$zj#"
//#define WZSEP		"$FF$"
//菜单字串分隔符，一次向客户端发送多个菜单项，每项之间用此连接，菜单名字和关联命令之间用 '|' 连接
#define WZSEP2		"$w2#"
#define WZSEP3		"$w3#"
#define WZSEP4		"$w4#"

//长描述换行符
#define WZBR		"$br#"
#define WZURL(w)	ESA + "[u:" + w + "]"
#define WZSIZE(n)	ESA + "[s:" + n + "]"


//让客户端把玩家登陆信息提交上来  好像没有用了...
#define WZ_GUINF	ESA + "200"

//向上浮动显示的文字提示框
#define WZ_NOTICE   ESA + "201"

//输入框  INPUTTXT("标题","你共有"+obj->short()+"，你想存多少？","按钮1:cmds"WZSEP2"按钮2:cmds")+NOR2
//INPUTTXT("标题","描述","cun $txt# "+ arg,"按钮")+NOR2
#define INPUTTXT(a,b,c)	ESA + "202" + a + WZSEP + b + WZSEP + c


	//场景名称标志，必须在行首，之后跟随的直到 '\n' 之前的内容将在客户端场景标题标签上显示，支持彩色显示
	//WZTITLE+ob->query("short")+"\n"....客户端收到WZTITLE标志设置标题的同时会清空出口和左侧场景物件栏

//场景标题
#define WZTITLE		ESA + "203"
//增加出口
#define WZEXIT		ESA + "204"
//清除出口
#define WZEXITRM		ESA + "205"
//没用?
#define WZEXITCL		ESA + "206"


//场景除了标题还有场景描述，就是他拉
#define WZLONG			ESA + "207"

//更新NPC 按钮 没有就新增 有的话就不增加
//WZOBIN"张三:look xx/xxx/cc#123"WZSEP"张三2:look xx/xxx/cc#123"NOR
#define WZOBIN			ESA + "208"
//客户端收到此消息。会将删除左侧栏对应物件。
////WZOBOUT"张三:look xx/xxx/cc#123"WZSEP"张三2:look xx/xxx/cc#123"NOR
#define WZOBOUT			ESA + "209"

//要求客户端发送用户登录信息
#define WZ_OP_LG 		ESA + "210"

//要求客户端弹出用户角色选择
#define WZ_OP_RG 		ESA + "211"

	//推送自定义按钮定义，必须在行首，之后跟随的直到 '\n' 之前的内容将在客户端自定义按钮区处理，类似出口处理
	//WZBTSET+"b1:查看状态:score"WZSEP"b2:查看技能:skills"..."\n"
	//可以任意推送任何一个按钮不必每次全部推送，总共17个自定义按钮，分别b1-b17表示。(bxx:名称:命令)
	//目前是用在底部按钮
#define WZBTSET			ESA + "006"
	//弹出描述标志，必须在行首，之后跟随的直到 '\n' 之前的内容将在客户端弹出框中显示，支持WZBR换行
	//此标志打头的文字信息会弹出显示，设计中可以配合文字点击协议灵活运用
	//WZOBLONG"这是一个巨大的石头(stone)，你可以试着"+WZURL("cmds:move stone")+"推动(move)"NOR"它。\n"
#define WZOBLONG		ESA + "007"
	//弹出窗口中的附加列表框，必须在行首，之后跟随的直到 '\n' 之前的内容将在客户端弹出框中的列表区中处理，作为可触摸命令按钮显示
	//次接口与文字点击接口功能类似，但是必须与WZOBLONG结合，可以根据需要灵活调用此接口或者文字点击接口。


//-------------结合 WZOBINFO 使用的
//用于底部按钮1区显示
#define WZOBACTS		ESA + "008"

//用于底部按钮2区显示
#define WZOBACTS2		ESA + "009"

//用于属性显示
#define WZOBPROPS		ESA + "081"

//用于信息显示
#define WZOBINF			ESA + "082"

//用于顶部的横向滚动BOX
#define WZOBHSVG		ESA + "083"
//-------------结合 WZOBINFO 完

//弹出技能面板 技能面板专用---------------------------
#define WZOBSKINF		ESA + "084"

#define WZOBSKSVG		ESA + "085"
#define WZOBSKACTS		ESA + "086"
#define WZOBSKACTS2		ESA + "087"

//更新技能面板
#define WZUPSKINF		ESA + "088"




//弹出列表面板 列表类型数据专用
#define WZOPLIST		ESA + "089"
//顶部两边的按钮
#define WZLISTSVG		ESA + "090"

//用于在列表面板的列表上面显示的一个区域，最多4个按钮
// WZLISTTAGBOX""文字描述"WZSEP"按钮1:cmds"WZSEP"按钮2:cmds"WZSEP"按钮3:cmds"WZSEP"按钮4:cmds"NOR2
#define WZLISTTAGBOX		ESA + "105"

#define WZLISTIETM		ESA + "091"
#define WZLISTBBYUT		ESA + "092"

//更新列表面板
#define WZUPLIST		ESA + "093"




//装备面板  6个区域
//打开装备面板

#define WZOPEQU			ESA + "094"
//顶部的按钮
#define WZEQUSVG		ESA + "095"
//左边的按钮
#define WZEQULBUT		ESA + "096"
//中间的描述文字 支持上下滚动 富文本
#define WZEQUCTXT		ESA + "097"
//右边的按钮
#define WZEQURBUT		ESA + "098"
//底部的文字 支持上下滚动 富文本
#define WZEQUBTXT		ESA + "099"
//最底部的按钮
#define WZEQUBBUT		ESA + "101"
//更新装备面板
#define WZUPEQU			ESA + "102"
//装备面板完---------------------------------------


//主界面 任务区域 格式为 WZTASK"任务:cmds"WZSEP"任务标题""WZSEP"任务描述"
#define WZTASK 			ESA + "103"

//聊天界面绑定协议  WZCHATBOX"闲聊:chat $txt#"WZSEP"谣言:rumor $txt#"WZSEP"指令:$txt#"NOR2
#define WZCHATBOX  		ESA + "104"

//出口区右边的三个按钮 第一个按钮的指令无效，默认是客户端自己的事件
//WZEXITRBUT切:0:cmds"WZSEP图:0:cmds"WZSEP乘雕:0:cmds"NOR2
//中间的0值表示该按钮是否选中选中状态 0 正常，1 闪烁文字大小 2 选中
#define WZEXITRBUT 	    ESA + "106"

//覆盖在方向键区的用于额外的信息显示的文本区
//按钮的属性 中间的0值表示该按钮是否选中选中状态 0 正常，1 闪烁文字大小 2 选中
//WZEXITCTXTBOX"左边的文字支持更新.."WZSEP"右边的文字支持更新"WZSEP"渡劫:0:cmds"WZSEP2"详情:0:cmds"
#define WZEXITCTXTBOX 	    ESA + "107"

//当顶部不使用血条 用文本显示时，需要该协议
//WZATTRTXTBOX"该内容支持更新...."  支持更新
#define WZATTRTXTBOX	    ESA + "108"



//WZAPPINIT 是否播放背景音乐，是否播放战斗音乐,音乐下载地址，
/* 		0:0 第一个数字表示是否播放背景0 不播放 1 播放 第二个数字表示是否播放战斗音乐 0 不播放 1 播放 默认不播放
		下载地址： 1是BGM 2是战斗音乐
		参数3 顶部使用血条还是一个长条的富文本框 1血条，2 富文本框 默认是血条
		参数4 APP布局 1 默认布局 2流水布局  默认使用默认的布局
  WZAPPINIT"0:0"WSZEP"1:xxxx.com/mp3"WSEP2"2:xxxx.com/mp3"WZSEP"2"WZSEP"1"
*/
#define WZAPPINIT 	    ESA + "109"

//战斗场景b区
#define WZBTEAREAB 	    	ESA + "110"

//战斗场景中的排行榜
#define WZBTERANK    		ESA + "111"
//战斗场景中的排行榜
//查看观战玩家
#define WZBATTLE_OBER    	ESA + "116"

//音乐开关 0关 1开
#define WZSOUND    		ESA + "112"


#define WZYESNO			ESA + "010" //暂时没有用

//必须在行首，之后跟随的直到 '\n' 之前的内容将在客户端地图框中显示，地图框支持WZBR换行
//WZMAPTXT"标题"WZSEP"地图字符串"NOR2
#define WZMAPTXT		ESA + "011"

#define WZHPTXT			ESA + "012"
//配合start_more()使用，start_more()输出的内容将在客户端大文本阅读框中分页显示，支持WZBR换行
#define WZMORETXT		ESA + "013"
//让客户端执行指令
#define WZFORCECMD(c)	ESA + "014" + c + "\n"
//既显既隐提示标志，必须在行首，之后跟随的直到 '\n' 之前的内容将在客户端临时消息框中渐显渐隐显示
#define WZTMPSAY		ESA + "015"

/**
 * WZ USER AREA BUT 用户按钮区的按钮
 * 用户区的按钮被点击后，客户端所有弹出的面板都要关闭，这个一定要注意
 * 因为面板中的按钮能绑定 关闭或者不关闭面板自身的事件，这种和面板没有关系的按钮
 * 都需要直接关闭弹出面板，包括底部的功能按钮也是这样
 */
#define WZUAREABUT		ESA + "019"

#define WZPOPMENU		ESA + "020"
//右边浮动按钮
#define WZRFBUT			ESA + "021"
//底部功能按钮
#define WZUBTSET		ESA + "022"
//标题栏右侧的按钮
#define WZBUTC			ESA + "023"

//信息面板
//w,h 百分比
// WZOBINFO"1,2"WZSEP"
//1 表示是否点了关闭面板 发送指定的指令？1为发送 0为不发送
//1:cmd"WZSEP"标题啊啊啊啊啊"WZSEP"
//介绍信息"WZSEP"  如果该分组为空，那就隐藏信息显示框
//属性部分 如果该分组为空，那就隐藏属性信息显示框  注意 属性为两列
//属性一WZSEP3血量:10000"WZSEP3"气血:10000"WZSEP3"内力:10000"WZSEP3"法力:10000"WZSEP2
//属性二WZSEP3攻击:10000"WZSEP3"战力:10000"WZSEP3"怒气:10000"WZSEP3"精力:10000"WZSEP
//底部按钮部分 x:是否点了按钮就关闭面板:指令  0为否 1为是
//1:拜师:cmdsWZSEP21:请教:cmdsWZSEP
//右边按钮部分  没有就隐藏 左边自动拉伸
//1:拜师:cmdsWZSEP21:请教:cmdsWZSEP

//WZOBINFO"1:2:0:cmds:我是标题NOR2
//WZOBINF"啊实打实大师阿萨德啊实打实"
//WZOBPROPS 没有属性客户端就要隐藏属性区域
/*
	属性一WZSEP3血量:10000"WZSEP3"气血:10000"WZSEP3"内力:10000"WZSEP3"法力:10000"WZSEP4
	属性一WZSEP3血量:10000"WZSEP3"气血:10000"WZSEP3"内力:10000"WZSEP3"法力:10000"WESEP

*/
//WZOBACTS"1:按钮1:cmds"WZSEP"1:按钮1:cmds"NOR2
//WZOBACTS2"1:按钮1:cmds"WZSEP"1:按钮1:cmds"NOR2



#define WZOBINFO		ESA + "024"


#define WZMENUF(r,w,h,s)	"$"+r+","+w+","+h+","+s+"#"

#define WZCHANNEL		ESA + "100"
	//显示血条这个是NPC的血条
#define WZATTR			ESA + "051"
// #define WZOPFGTBOX		ESA + "061" //打开全屏战斗界面
// #define WZCLFGTBOX		ESA + "062" //关闭全屏战斗界面

//创建战斗场景
#define WZOPBTE  ESA + "993"

//开始战斗
#define WZBTEFIGHT ESA + "994"
//结束战斗
#define WZSTOPBTEFIGHT ESA + "995"

//加入战斗
#define WZJOINBTEFIGHT ESA + "996"

//退出战斗
#define WZEXITBTEFIGHT ESA + "997"


//该协议表示 协议头之后的数据客户端都按照JSON格式处理
#define WZJSON			ESA+"998"

#define WZSYSEXIT		ESA + "999"
#define WZEC			"\n" //结束符


//允许进入主面板
#define WZSYS0007		    ESA + "0007"
//退出到登录界面
#define WZSYS1000			ESA + "1000"


//面板ID定义
#define INF_PANEL 	"infPanel"
#define LIST_PANEL 	"listPanel"
#define SK_PANEL 	"skPanel"
#define EQU_PANEL 	"equPanel"


#endif
