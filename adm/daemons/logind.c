// logind.c

#include <ansi.h>
#include <command.h>
#include <login.h>
#include <config.h>
#include <getconfig.h>
#include <protocol.h>
//#include "/u/mudren/mail_center/mail.h" // 包含读取邮件信息的函数

#define MUDLIST_CMD     "/cmds/usr/mudlist"
#define REBOOT_CMD      "/cmds/arch/reboot"

inherit F_DBASE;

nosave int wiz_lock_level = 0;

nosave string *movie;

string *banned_name = ({
    "你", "我", "他", "她", "它",
    "爸", "爷", "妈", "奶",
    "屎", "尿", "粪", "　",
});

//复姓列表 要求玩家输入角色名称时，名称前两个字包含在这个列表的就取名称的前两个字作为姓氏
string *comp_name = ({
    "欧阳","公孙","上官",
    "欧阳","太史","端木","上官","司马","东方","独孤","南宫","万俟",
    "闻人","夏侯","诸葛","尉迟","公羊","赫连","澹台","皇甫","宗政",
    "濮阳","公冶","太叔","申屠","公孙","慕容","仲孙","钟离","长孙",
    "宇文","司徒","鲜于","司空","闾丘","子车","亓官","司寇","巫马",
    "公西","颛孙","壤驷","公良","漆雕","乐正","宰父","谷梁","拓跋",
    "夹谷","轩辕","令狐","段干","百里","呼延","东郭","南门","羊舌",
    "微生","公户","公玉","公仪","梁丘","公仲","公上","公门","公山",
    "公坚","左丘","公伯","西门","公祖","第五","公乘","贯丘","公皙",
    "南荣","东里","东宫","仲长","子书","子桑","即墨","达奚","褚师","吴铭",
});


string *banned_id = ({
    "admin", "all", "api", "arch", "auth",
    "cancel", "exit", "guest",
    "name", "new", "none",
    "quit", "root", "user", "wizard",
});

// 内部调用的函数
private void get_id(string arg, object ob);
private void get_passwd(string pass, object ob);
private void get_ad_passwd(string pass, object ob);
private void get_character(string type, object ob);
private void check_ok(object ob);
private void confirm_id(string yn, object ob);
private void init_new_player(object user);
private void check_name(string arg, object ob);
private void init_role(string gender, object ob);

private void get_login_inf(string arg, object ob);


    // 可以被外部调用的函数
    object make_body(object ob);
    varargs void enter_world(object ob, object user, int silent);
    varargs void reconnect(object ob, object user, int silent);
    object find_body(string name);
    int check_legal_id(string arg);
    int check_legal_name(string arg, int maxlen);




void send_cmds(){



	 write(WZBTSET"b31:指令<br/>输入:appinput"
			 WZSEP"b32:角色<br/>信息:mycmds ofen"
			 WZSEP"b33:背包<br/>物品:i"
			 WZSEP"b34:技能<br/>相关:mycmds skill"
			 WZSEP"b35:战斗<br/>相关:mycmds fight"
			 WZSEP"b36:任务<br/>相关:mycmds quest"
			 WZSEP"b37:频道<br/>交流:liaotian"NOR2);

write(WZCHATBOX "闲聊:chat $txt#" WZSEP "谣言:rumor $txt#" WZSEP "指令:$txt#" NOR2);

	write(WZBUTC
			  				"首冲:shop"WZSEP
							"礼包:mygift duo"WZSEP
							"福利:mycmds sgin"WZSEP
                            "帮派:mycmds sgin"WZSEP
                            "宗门:mycmds sgin"WZSEP
                            "百度:mycmds sgin"WZSEP
			  				"网易:help hj"NOR2);

				write(WZRFBUT
			  				"商城:shop"WZSEP
							"夺宝:mygift duo"WZSEP
							"签到:mycmds sgin"WZSEP
			  				"设置:help hj"NOR2);
        write(WZFORCECMD("look")+NOR2);







}




void create()
{
    seteuid(ROOT_UID);
    set("channel_id", "连线精灵");
    set("name", HIW "连线精灵" HIM);
    if (intp(WIZ_LOCK_LEVEL))
        wiz_lock_level = WIZ_LOCK_LEVEL;
    else
        wiz_lock_level = 0;
}

void logon(object ob)
{
    object *usr;
    int i, wiz_cnt, ppl_cnt, login_cnt;
    int iplimit;
    //int user_num;

    if (BAN_D->is_banned(query_ip_number(ob)) == 1)
    {
        write("你的地址在本 MUD 不受欢迎。\n");
        destruct(ob);
        return;
    }



    if (! VERSION_D->is_version_ok() && ! VERSION_D->query_temp("operator"))
        write(HIY "现在本站正在同步版本，如果你不是巫师，请稍候再登录。\n\n" NOR);
    else if (REBOOT_CMD->is_rebooting())
        write(HIY "现在本站正在准备重新启动，如果你不是巫师，请稍候再登录。\n\n" NOR);

    usr = users() + filter(children(LOGIN_OB), (: interactive :));
    wiz_cnt = 0;
    ppl_cnt = 0;
    login_cnt = 0;
    iplimit = 0;
    for(i = 0; i < sizeof(usr); i++)
    {
        if (query_ip_number(usr[i]) == query_ip_number(ob)) iplimit++;
        if (base_name(usr[i]) == LOGIN_OB) login_cnt++;
        else if (wizardp(usr[i]))
        {
            if (! usr[i]->query("env/invisible")) wiz_cnt++;
        }
        else ppl_cnt++;
    }

#if 0
    if (iplimit > 9)
    {
        write("对不起，" + LOCAL_MUD_NAME() + "限制相同ip多重登录。\n");
        destruct(ob);
        return;
    }
#endif

    // if (iplimit > 1)
    //     printf("您所在的地址已有 " HIY "%d" NOR " 位玩家在线上。\n", iplimit-1);

    // printf("目前共有 " CYN "%d" NOR " 位巫师、"
    //         CYN "%d" NOR " 位玩家在线上，以及 "
    //         CYN "%d" NOR " 位使"
    //         "用者尝试连线中。\n\n", wiz_cnt, ppl_cnt, login_cnt);

    //要求客户端发送登录信息！

    write(WZ_OP_LG+"\n");
    input_to("get_id", ob);

}

private void get_id(string arg, object ob)
{
    object ppl;

    string *s = explode(arg, ",");
    string arg1 = arg;

    arg = s[0];



    arg = trim(lower_case(arg));

    if (! check_legal_id(arg))
    {
        write(WZ_NOTICE"请重新输入账号");
        input_to("get_id", ob);
        return;
    }

    // 检查最大连接人数
    if (intp(MAX_USERS) && MAX_USERS > 0)
    {
        if ((string)SECURITY_D->get_status(arg) == "(player)" && sizeof(users()) >= MAX_USERS)
        {
            ppl = find_body(arg);
            // Only allow reconnect an interactive player when MAX_USERS exceeded.
            if (! ppl)
            {
                write(WZ_NOTICE"对不起，" + LOCAL_MUD_NAME() + "的使用者已经太多了，请待会再来。\n"NOR2);
                destruct(ob);
                return;
            }
        }
    }

    if (wiz_level(arg) < 1)
    {
        if (! VERSION_D->is_version_ok() && ! VERSION_D->query_temp("operator"))
        {
            write(WZ_NOTICE"现在本站正在同步版本中，暂时不能登录，请稍候再尝试。\n"NOR2);
            destruct(ob);
            return;
        } else if (REBOOT_CMD->is_rebooting() && ! find_player(arg)){
            write(WZ_NOTICE"现在本站正准备重新启动，暂时不能登录，请稍候再尝试。\n"NOR2);
            destruct(ob);
            return;
        }
    }

    if ((string)ob->set("id", arg) != arg)
    {
        write(WZ_NOTICE"绑定账号ID时系统错误,请联系管理员！\n"NOR2);
        destruct(ob);
        return;
    }

    if (arg == "new" || arg == "guest"){
        // If guest, refuse them create the character.
        confirm_id("no", ob);
        return;
    } else if (file_size(ob->query_save_file() + __SAVE_EXTENSION__) >= 0){

        //有账号信息 开始验证密码是否正确
        if (ob->restore()){


            if(  ob->query("password")!=crypt(s[1], WZKEY)) {
                //密码不匹配！
                write(WZ_NOTICE"密码错误请重新登录！\n"NOR2);
                //再次要求发送登录信息
                write(WZ_OP_LG+"\n"NOR2);
                input_to("get_id", ob);
                return;
            }else{
                //这儿是进入游戏世界了

                check_ok(ob);
                return;
            }

           return;
        }
        write(WZ_NOTICE"您的人物储存档出了一些问题，请联系管理员！\n"NOR2);
        destruct(ob);
        return;
    }



    //开始走创建角色的流程
    //检查密码是否正常

    if ( strlen( s[1] ) < 3 ){
        write(WZ_NOTICE"密码长度不能少于三个字哟！\n"NOR2);
        write(WZ_OP_LG+"\n"NOR2);
        input_to("get_id", ob);
        return;
    }



     ob->set("password", crypt(s[1], WZKEY));
     ob->seve();


     write(WZ_OP_RG + "\n"NOR2);
     input_to("create_role_name", ob);

}





//新注册登录专用
private void check_login_inf(string arg, object ob)
{
    string fname;
    string result;

    ob->set("purename", arg);

    fname = ob->query("surname");
    if (! stringp(fname)) fname = "";
    fname += arg;

    if (strlen(fname) < 2)
    {
        write(WZ_NOTICE"角色名称至少两个汉字哦！\n"NOR2);
        //要求重来
        input_to("set_role_name", ob);
        return;
    }

    if ( stringp( result = NAME_D->invalid_new_name(fname) ) || stringp(result = NAME_D->invalid_new_name(arg))){
        write(WZ_NOTICE"对不起，" + result+NOR2);
        input_to("set_role_name", ob);
        return;
    }
    init_role(arg,ob);
}

/**
 *
 * 新版专用
 *
 */
void create_role_name(string arg, object ob){
    //性别，角色名称
    string *s =   explode(arg, ",");
    string sn,sex;

    //s[0]是性别
    //s[1]是全名
    sn = s[1][0..1];
    //单姓
    if( member_array( sn, comp_name ) < 0 ){
        ob->set("surname",s[1][0..0]);
        //名字
        sn = s[1][1..strlen(s[1]) - 1];
    }else{
        //复姓
        ob->set("surname",s[1][0..1]);
        //名字
        sn = s[1][2..strlen(s[1])];
    }

    if( s[0]=="男" ){
        sex = "男性";
    }else{
        sex = "女性";
    }

    ob->set_temp("gender", sex);
    check_login_inf(sn, ob);
}







/**
 * 新版专用的角色初始化
 */
private void init_role(string gender, object ob)
{
    object user;



    if (find_body(ob->query("id")))
    {
        write(WZ_NOTICE+HIR "这个玩家现在已经登录到这个世界上了，请"
              "退出重新连接。\n"NOR2);
        destruct(ob);
        return;
    }

    ob->set("body", USER_OB);
    if (! objectp(user = make_body(ob)))
    {
        write(WZ_NOTICE+HIR "你无法登录这个新的人物，请重新选择。\n"NOR2);
        destruct(ob);
        return;
    }
    user->set("str", 14);
    user->set("dex", 14);
    user->set("con", 14);
    user->set("int", 14);
    user->set("per", 20);
    user->set("character", ob->query_temp("type"));
    user->set("gender", ob->query_temp("gender"));
    ob->set("registered", 0);
    user->set("registered", 0);
    // log_file("usage", sprintf("%s(%s) was created from %s (%s)\n",
    //         user->query("name"), user->query("id"),
    //         query_ip_number(ob), ctime(time())));

    ob->save();

    init_new_player(user);

	write(WZ_NOTICE"恭喜！角色创建成功！\n"NOR2);
	//0007，登陆成功
	write(WZSYS0007"\n"NOR2);



    enter_world(ob, user);

}






private void check_ok(object ob)
{
    object user;

    // Check if we are already playing.
    user = find_body(ob->query("id"));
    if (user)
    {
        if (user->is_net_dead())
        {
            reconnect(ob, user);
            return;
        }
        write(WHT "\n您要将另一个连线中的相同人物赶出去，取而代之吗？("
              HIY "y/n" NOR + WHT ")" NOR);
        input_to("confirm_relogin", ob, user);
        return;
    }

    user = MESSAGE_D->find_chatter(ob->query("id"));
    if (objectp(user))
    {
        write("你把正在聊天的ID踢了出去。\n");
        MESSAGE_D->user_logout(user, user->name(1) + "从" +
                   query_ip_number(ob) + "连线进入世界，"
                   "把你踢了出去。\n");
    }

    if (objectp(user = make_body(ob)))
    {
        if (user->restore())
        {
            mixed err;
            string msg;

            log_file("usage", sprintf("%s(%s) loggined from %s (%s)\n",
                    user->query("name"), user->query("id"),
                    query_ip_number(ob), ctime(time()) ) );


            // if (ob->query("last_on") <= time() &&  ob->query("last_on") > time() - 30 && ! wiz_level(user))
            // {
            //     write(WHT "\n你距上次退出只有" HIY + chinese_number(time() - ob->query("last_on")) +
            //           NOR + WHT "秒钟，请稍候再登录。\n" NOR);
            //     destruct(user);
            //     destruct(ob);
            //     return;
            // }

            user->set_temp("logon_time", time());
            if (err = catch(enter_world(ob, user)))
            {
                user->set_temp("error", err);
                msg = HIR "\n进入这个世界时出了一些问题，需要和巫师联系处理。\n\n" NOR;
                if (mapp(err))
                    msg += MASTER_OB->standard_trace(err, 1);
                user->set_temp("error_message", msg);
                tell_object(user, msg);
            }

            write(WZSYS0007"\n");

            return;
        } else
        {
            destruct(user);
            write(HIR "\n无法读取你的数据档案，您需要和巫师联系。\n" NOR);
            if (CONFIG_D->query_int("ask_recreate"))
            {
                write(WHT "你可以选择重新创造玩家(y/n)：" NOR);
                input_to("create_new_player", ob);
            } else
            destruct(ob);
        }
    } else
    write(HIR "无法创建该玩家，你可以尝试重新登录或是和巫师联系。\n" NOR);
}



private void confirm_relogin(string yn, object ob, object user)
{
    object old_link;

    if (! yn || yn == "")
    {
        write(WHT "\n您要将另一个连线中的相同人物赶出去，取而代之吗？("
              HIY "y/n" NOR + WHT ")" NOR);
        input_to("confirm_relogin", ob, user);
        return;
    }

    if (yn[0]!='y' && yn[0]!='Y')
    {
        write("好吧，欢迎下次再来。\n");
        destruct(ob);
        return;
    }

    if (user)
    {
        tell_object(user, "有人从别处( " + query_ip_number(ob)
            + " )连线取代你所控制的人物。\n");
        log_file("usage", sprintf("%s(%s) replaced by %s (%s)\n",
           user->query("name"), user->query("id"),
           query_ip_number(ob), ctime(time())));

        // Kick out tho old player.
        old_link = user->query_temp("link_ob");
        if (old_link)
        {
            exec(old_link, user);
            destruct(old_link);
        }
    } else
    {
        write("在线玩家断开了连接，你需要重新登陆。\n");
        destruct(ob);
        return;
    }


    write(WZSYS0007"\n"NOR2);
	write(WZ_NOTICE"登录成功，正在加载世界。。。\n"+NOR2);
    reconnect(ob, user);
}











object make_body(object ob)
{
    string err;
    object user;
    //int n;

    if (! is_root(previous_object()))
    {
        log_file("static/security",
        sprintf("%s try to create player(%s) on %s.\n",
               (string)geteuid(previous_object()),
               (string)ob->query("id"),
               ctime(time())));
        write("你没有权限创建玩家。\n");
        return 0;
    }

    user = new(USER_OB);
    if (! user)
    {
        write("现在可能有人正在修改使用者物件的程式，无法进行复制。\n");
        write(err+"\n");
        return 0;
    }
    // 设置玩家对象ID
    seteuid(ob->query("id"));
    export_uid(user);
    export_uid(ob);
    seteuid(getuid());

    user->set("id", ob->query("id"));
    user->set("surname", ob->query("surname"));
    user->set("purename", ob->query("purename"));
    user->set_name( 0, ({ ob->query("id")}) );
    return user;
}

private void init_new_player(object user)
{
    // 初始化必要属性
    user->set("title", "普通百姓");
    user->set("birthday", time() );
    user->set("potential", 99);
    user->set("food", (user->query("str") + 10) * 10);
    user->set("water", (user->query("str") + 10) * 10);
    user->set("channels", ({ "chat", "rumor", "party",
         "bill", "sing", "family", "rultra" }));

    // 记录名字
    NAME_D->map_name(user->query("name"), user->query("id"));

    // 设置必要的环境参数
    user->set("env/auto_regenerate", 1);
    user->set("env/auto_get", 1);
    user->set("env/wimpy", 60);
    //设定不自动转宗师频道，防止困扰玩家 by 薪有所属
    user->set("env/no_autoultra", 1);
}

varargs void enter_world(object ob, object user, int silent)
{
    object cloth, shoe;
    string startroom;
    string ipname;
    // int new_mail_n;
    object sob; // 检查身上物品是否过多，查询是否有作弊嫌疑
    int i;
    string *sobs = ({
          "xisui xiandan",
          "wuji xiandan",
           "tonghui xiandan",
          "zhuyuan xiandan",
          "wuhua guo",
    });

    if (! is_root(previous_object()))
        return;

    user->set_temp("link_ob", ob);
    ob->set_temp("body_ob", user);
    ob->set("registered", user->query("registered"));
    if (interactive(ob)) exec(user, ob);

    write("\n目前权限：" + wizhood(user) + "\n");

    user->setup();
    if (user->query("age") == 14)
    {
        user->set("food", user->max_food_capacity());
        user->set("water", user->max_water_capacity());
    }

    // In case of new player, we save them here right aftre setup
    // compeleted.
    user->set("last_save", time());
    user->save();
    ob->save();

    //给地图
    user->set("map_all", 1);



    // check the user
    UPDATE_D->check_user(user);

    if (wizhood(user) == "(player)")
    {
        if (user->query("class") == "bonze")
        {
            if (user->query("gender") == "女性")
            {
                cloth = new("/clone/cloth/ni-cloth.c");
                shoe = new("/clone/cloth/ni-xie.c");
            }
            else
            {
                cloth = new("/clone/cloth/seng-cloth.c");
                shoe = new("/clone/cloth/seng-xie.c");
            }
        } else
        {
            if (user->query("class") == "taoist")
            {
                if (user->query("gender") == "女性")
                {
                    cloth = new("/clone/cloth/daogu-cloth.c");
                    shoe = new("/clone/cloth/dao-xie.c");
                } else
                {
                    cloth = new("/clone/cloth/dao-cloth.c");
                    shoe = new("/clone/cloth/dao-xie.c");
                }
            } else
            {
                if (user->query("gender") == "女性")
                {
                    shoe = new("/clone/cloth/female-shoe.c");
                    cloth = new(sprintf("/clone/cloth/female%d-cloth.c", 1 + random(8)));
                } else
                {
                    shoe = new("/clone/cloth/male-shoe.c");
                    cloth = new(sprintf("/clone/cloth/male%d-cloth.c", 1 + random(8)));
                }
            }
        }

        cloth->move(user);
        catch(cloth->wear());
        shoe->move(user);
        catch(shoe->wear());
    }
    else
    {
        if (user->query("gender") == "女性")
        {
            cloth = new("/clone/cloth/yunshang");
            cloth->move(user);
            cloth->wear();
        } else
        {
            cloth = new("/clone/cloth/jinduan");
            shoe  = new("/clone/cloth/xianlv");
            cloth->move(user);
            cloth->wear();
            shoe->move(user);
            shoe->wear();
        }
    }

    if (cloth && (! environment(cloth) || ! cloth->query("equipped")))
        destruct(cloth);

    if (shoe && (! environment(shoe) || ! shoe->query("equipped")))
        destruct(shoe);

    user->set("registered", 1);
    // user->set("born",1);
    if (! silent)
    {
        color_cat(MOTD);

        write("你连线进入" + LOCAL_MUD_NAME() + "。\n\n");
        if (!stringp(user->query("character")))
        {
            if (user->is_ghost())
                user->reincarnate();
            user->set("startroom", REGISTER_ROOM);
        }
        else if (! stringp(user->query("born")))
        {
            if (user->is_ghost())
                user->reincarnate();
            user->set("startroom", BORN_ROOM);
        }

        if (user->is_in_prison())
            startroom = user->query_prison();
        else if (user->is_ghost())
            startroom = DEATH_ROOM;
        else if (! stringp(startroom = user->query("startroom")) ||
            file_size(startroom + ".c") < 0)
            startroom = START_ROOM;

        if (! catch(load_object(startroom)))
            user->move(startroom);
        else
        {
            user->move(START_ROOM);
            startroom = START_ROOM;
            user->set("startroom", START_ROOM);
        }
        tell_room(startroom, user->query("name") +
            "连线进入这个世界。\n", ({user}));
    }

    if (ob->query("registered"))
    {
        if (!(ob->query("login_times")))
        {
            write(NOR "\n欢迎回到" + LOCAL_MUD_NAME() + "。\n");
            // show rules
            color_cat(UNREG_MOTD);
            ob->set("login_times", 1);
            // MYSQL_D->register(ob);
        }
        else
        {
            ob->add("login_times", 1);
            write(NOR "\n你是第 " + ob->query("login_times") + " 次光临" + LOCAL_MUD_NAME() + "。\n");
            write("\n你上次是 " + HIG + ctime(ob->query("last_on")) + NOR + " 从 " + HIR +
                    ob->query("last_from") + NOR + " 连接的。\n");
        }
    }

    // 检查同盟情况
    "/cmds/usr/league"->main(this_player(), "check");

    for (i = 0; i < sizeof(sobs); i ++)
    {
        if (wizardp(this_player())) break;

        if (objectp (sob = present(sobs[i], this_player())) )
        {
            if (sob->query_amount() > 99)
                log_file("warning", this_player()->name() + "(" +
                      this_player()->query("id") + ") has more than" + " " +
                      sob->query_amount() + " "+ sobs[i] + "\n");

            if (sob->query_amount() > 999)
            {
                log_file("logind_throw", this_player()->name() + " had been throwed by 连线精灵。\n");
                this_player()->get_into_prison(this_object(), 0, 144400);
                break;
            }
        }
    }

    // if (ob->query("ad_password")[0..2]!= "$6$")
    // {
    //     tell_object(this_player(), HBRED "\n你的管理密码没有升级为SHA512加密，为了账号安全请使用" HIY " passwd " NOR HBRED "修改密码。" NOR "\n");
    // }
    // if (ob->query("password")[0..2] != "$6$")
    // {
    //     tell_object(this_player(), HBRED "\n你的登录密码没有升级为SHA512加密，为了账号安全请使用" HIY " passwd " NOR HBRED "修改密码。" NOR "\n");
    // }
    // write("\n");
    /*
    // 检查是否有新邮件未读
    new_mail_n = get_info(user->query("id"), "newmail", "", 0);

    if (new_mail_n)
        tell_object(user, HIG"【炎黄邮件系统】：你有 "
                    HIY + new_mail_n + HIG " 封新邮件，请到邮件中心查阅！\n" NOR);
    */
    CHANNEL_D->do_channel(this_object(), "sys",
    sprintf("%s(%s)由[%s]连线进入。",
            user->name(), user->query("id"),
            (ipname = query_ip_number(user)) ? ipname : "未知地点"));

#if INSTALL_EXAMINE
    EXAMINE_D->examine_player(user);
#endif

    if (wizhood(user) != "(admin)" &&
      EXAMINE_D->query("log_by/" + user->query("id")))
        user->start_log();

    // notice user the user of this mud
    //不发送新闻
    //NEWS_D->prompt_user(user);

    if ((user->query("qi") < 0 || user->query("jing") < 0) && living(user))
        user->unconcious();

    send_cmds();

}

varargs void reconnect(object ob, object user, int silent)
{
    //int new_mail_n;
    object sob; // 检查身上物品，是否有作弊嫌疑
    int i;
    string *sobs = ({
          "xisui xiandan",
          "wuji xiandan",
          "tonghui xiandan",
          "zhuyuan xiandan",
          "wuhua guo",
    });

    user->set_temp("link_ob", ob);
    ob->set_temp("body_ob", user);
    exec(user, ob);

    user->reconnect();


    if( !silent && (! wizardp(user) || ! user->query("env/invisible"))) {
        tell_room(environment(user), user->query("name") + "重新连线回到这个世界。\n",
                 ({user}));
    }

    // 检查同盟情况
    "/cmds/usr/league"->main(this_player(), "check");

    for (i = 0; i < sizeof(sobs); i ++)
    {
        if (wizardp(this_player())) break;

        if (objectp (sob = present(sobs[i], this_player())) )
        {
            if (sob->query_amount() > 50)
                log_file("warning", this_player()->name() + "(" +
                      this_player()->query("id") + ") has more than" + " " +
                      sob->query_amount() + " " + sobs[i] + "\n");
            if (sob->query_amount() > 100)
            {
                log_file("logind_throw", this_player()->name() + " had been throwed by 连线精灵。\n");
                this_player()->get_into_prison(this_object(), 0, 144400);
                break;
            }
        }
    }

    // tell_object(this_player(), HIG "\n今后请使用" HIY " wenxuan " HIG "命令查阅游戏的文章选集。\n\n");
    /*
    // 检查是否有新邮件未读
    new_mail_n = get_info(user->query("id"), "newmail", "", 0);

    if (new_mail_n)
        tell_object(this_player(), HIG"【炎黄邮件系统】：你有 "
                    HIY + new_mail_n + HIG " 封新邮件，请到邮件中心查阅！\n" NOR);
    */
    CHANNEL_D->do_channel( this_object(), "sys",
        sprintf("%s(%s)由[%s]重新连线进入。", user->query("name"),
        user->query("id"), query_ip_number(user)));


    send_cmds();

}

int check_legal_id(string id)
{
    int i;
    /*
    if (id == "all")
    {
        write(WHT "\n对不起，[" HIC "all" NOR + WHT "]这个词有着特"
              "殊的含意，不能用做英文名字。\n" NOR);
        return 0;
    }
    */
    if (member_array(id, banned_id) != -1)
    {
        write(WZ_NOTICE"对不起，" HIC"“" + id + "”" NOR"这个词有着特"
            "殊的含意，不能用做英文名字。\n"NOR2);
        return 0;
    }

    i = strlen(id);

    if ((i < 3) || (i > 10))
    {
        write(WZ_NOTICE"对不起，你的英文名字必须是" HIY " 3 " NOR +
              WHT "到" HIY " 10 " NOR + WHT "个英文字母。\n" NOR+NOR2);
        return 0;
    }

    // while (i--)
    // {
    //     if (id[i] < 'a' || id[i] > 'z' )
    //     {
    //         write(WZ_NOTICE"对不起，你的英文名字只能用英文字母。\n" NOR+NOR2);
    //         return 0;
    //     }
    // }

    return 1;
}

int check_legal_name(string name, int maxlen)
{
    int i;

    i = strlen(name);

    if (! is_chinese(name))
    {
        write(WHT "对不起，请您用「" HIY "中文" NOR + WHT
                  "」取名字。\n" NOR);
        return 0;
    }

    if ((i < 1) || (i > maxlen))
    {
        write(WHT "对不起，你的中文姓名不能太长。\n" NOR);
        return 0;
    }

    if (member_array(name, banned_name) % 2 == 0)
    {
        write(WHT "对不起，这种姓名会造成其他人的困扰。\n" NOR);
        return 0;
    }

    return 1;
}

object find_body(string name)
{
    return find_player(name);
}

int set_wizlock(int level)
{
    if (wiz_level(this_player(1)) <= level)
        return 0;

    if (geteuid(previous_object()) != ROOT_UID)
        return 0;

    wiz_lock_level = level;
    return 1;
}
