// fly.c

#include <ansi.h>

inherit F_CLEAN_UP;
void fly_fin(string where,object me,object room);
int help(object me);

mapping places = ([
	"yangzhou" : "/d/city/guangchang",
	"gc"       : "/d/city/guangchang",
	"beijing"  : "/d/beijing/tiananmen",
	"chengdu"  : "/d/city3/guangchang",
	"changan"  : "/d/changan/bridge2",
	"suzhou"   : "/d/suzhou/canlangting",
	"hangzhou" : "/d/hangzhou/road10",
	"ouyang"   : "/d/baituo/damen",
	"fuzhou"   : "/d/fuzhou/dongjiekou",
	"foshan"   : "/d/foshan/street3",
	"guiyun"   : "/d/guiyun/taihu",
	"heimuya"  : "/d/heimuya/up1",
	"wugong"   : "/d/quanzhen/zhongxin",
	"taishan"  : "/d/taishan/taishanjiao",
	"dali"     : "/d/dali/center",
	"nanyang"  : "/d/shaolin/nanyang",
	"quanzhou" : "/d/quanzhou/zhongxin",
	"jiaxing"  : "/d/quanzhou/jiaxing",
	"xiangyang": "/d/xiangyang/guangchang",
	"yongdeng" : "/d/huanghe/yongdeng",
	"lingzhou" : "/d/lingzhou/center",
	"henyang"  : "/d/henshan/hengyang",
	"guanwai"  : "/d/guanwai/jishi",
	"xingxiu"  : "/d/xingxiu/xxh1",
	"baituo"   : "/d/baituo/guangchang",
	"quanzhen" : "/d/quanzhen/damen",
	"zhongnan" : "/d/quanzhen/shanjiao",
	"gumu"     : "/d/gumu/mumen",
	"murong"   : "/d/yanziwu/hupan",
	"lingjiu"  : "/d/lingjiu/jian",
	"shaolin"  : "/d/shaolin/shanmen",
	"wudang"   : "/d/wudang/jiejianyan",
	"xiaoyao"  : "/d/xiaoyao/xiaodao4",
	"huashan"  : "/d/huashan/shaluo",
	"huashanjianzong": "/d/huashan/pingdi",
	"xueshan"  : "/d/xuedao/nroad4",
	"xuedao"   : "/d/xuedao/wangyougu",
	"kunlun"   : "/d/kunlun/klshanlu",
	"emei"     : "/d/emei/huayanding",
	"mingjiao" : "/d/mingjiao/shanjiao",
	"nanhai"   : "/d/xiakedao/haibin",
	"hujia"    : "/d/guanwai/xiaoyuan",
	"yanziwu"  : "/d/yanziwu/bozhou",
	"pingan"  : "/d/pingan/index",
	"xiakedao"  : "/d/xiakedao",
	"yihua"  : "/d/yihua/damen",
	"taohua"  : "/d/taohua/damen",
	"shenlong"  : "/d/shenlong/damen",
	//"tangmen" :"/d/tangmen/zhongxin",
	//"baiyun" :"/d/baiyun/beimen",
	//"wudu" :"/d/dali/banshan",
	"mojiao" :"/d/xingxiu/shanjiao",
	"zhanchang":"/d/day_task/xiongnu/room1",
	"pk":"/d/pk/entry",
]);

int main(object me, string arg)
{
	object flying, room;
	string where;

	if (! arg) return help(me);
	if (! SECURITY_D->valid_grant(me, "(admin)"))
	{
		if (me->over_encumbranced())
				return notify_fail("你的负荷过重，动弹不得。\n");

			if (me->query_encumbrance() < 0)
				return notify_fail("你的负荷出现故障，动弹不得。\n");
	}




	if (me->query_temp("sleeped"))
		return notify_fail("你现在正躺着呢。\n");


	if (me->is_ghost())
		return notify_fail("等你还了阳再说吧。\n");

	if (me->is_in_prison())
		return notify_fail("你正在做牢呢，你想干什么？！\n");

	if (me->is_fighting())
		return notify_fail("你现在正在战斗！\n");

	if (me->is_busy() || me->query("doing"))
		return notify_fail("你的动作还没有完成，不能移动。\n");
	if(environment(me)->query("newplayer_room"))
	{
		return notify_fail(HIY"你还没有离开新手村呢，请先完成新手任务！\n"NOR);
	}
	/*
	if (! objectp(flying = me->query_temp("is_flying")))
		return notify_fail("你还没有激发御剑飞行状态！\n");

	if (! present(flying->query("id"), me))
		return notify_fail("你的飞剑不在你身边！\n");

	if (me->query_skill("fly", 1) < 5 )
		return notify_fail("你的御剑术不足以长途跋涉。\n");
		*/





	if (! environment(me)->query("outdoors"))
		return notify_fail("在房间里不能乱跑，到户外再说吧！\n");

	if (environment(me)->query("no_magic") || environment(me)->query("no_fly"))
		return notify_fail("你发现这里有点古怪，好像某些特殊神技没法使用~！\n");

	if (member_array(arg, keys(places)) == -1 && arg != "home")
		return notify_fail("这个地方无法飞过去。\n");


	if (arg == "home" && ! me->query("private_room/position"))
		return notify_fail("你还没有住房呢！\n");

	if (arg == "home" && stringp(me->query("private_room/position")) &&
	    file_size(me->query("private_room/position") + ".c") > 0)
		room = get_object(me->query("private_room/position"));
	else
	room = get_object(places[arg]);
	if (! room) return notify_fail("你感觉到似乎那个地方有点不对劲。\n");

	//message("vision", me->name() + "驾御「" + flying->name() + NOR "」直上云霄而去。\n",environment(me), ({me}));
	message("vision", me->name() + "驾御「大雕」，直上云霄，随风而去。\n",environment(me), ({me}));

	where = room->query("short");
	fly_fin(where,me,room);
//	call_out("fly_fin",1,where,me,room);
	//tell_object(me, HIG"\n\n你喃喃自语不知道念了一些什么 ~~"NOR"\n");
	//tell_object(me, "\n\n"HIM"顿时化为一道电光破天而起，飞向远方~~~"NOR"\n");
	return 1;
}
void fly_fin(string where,object me,object room)
{
	string omsg,mmsg;
	me->move(room);



	if ( to_int(me->query("combat_exp")) >=50000 )
	{	me->receive_damage("qi", 20);
		me->receive_damage("jing", 20);
	}

	tell_object(me, HIG"\n\n你乘雕一路飞行，终于赶到了" + where + "。"NOR"\n");
	if( me->query_skill("dodge",1) < 100 ){

		tell_object(me, HIG"只见你,打了个趔趄，摔到地上，搞个鼻青脸肿。"NOR"\n");
			message("vision", "只见空中出现一团黑影,"+me->name() + "从黑影中打了个趔趄，摔了下来，搞的鼻青脸肿像猪一样。"+ NOR+"\n",environment(me), ({me}));


	}else{

		tell_object(me, HIG"只见你,潇洒一跃，落到地上。"NOR"\n");
		message("vision", "只见空中出现一团黑影,"+me->name() + "从黑影中,潇洒的跳了出来。"+ NOR+"\n",environment(me), ({me}));

	}



	 MYGIFT_D->check_mygift(me, "newbie_mygift/find_pinan");


	me->delete_temp("is_flying");
}
int help(object me)
{
	write( L_ICON("05079"));
	write(@HELP
指令格式 : fly <地点>
目前可以利用乘雕去的地方有：
----------------------------------------------------------------------
pingan   : 平安镇
yangzhou : 扬  州	beijing  : 北  京	chengdu  : 成  都
changan  : 长  安	hangzhou : 杭  州	suzhou   : 苏  州
xiangyang: 襄  阳	fuzhou   : 福  州	foshan   : 佛  山
lingzhou : 灵  州	henyang  : 衡  阳	jiaxing  : 嘉  兴
quanzhou : 泉  州	dali     : 大  理	wudang   : 武当山
taishan  : 泰  山	quanzhen : 全真教	emei     : 峨眉山
mingjiao : 光明顶	xingxiu  : 星宿海	gumu     : 古  墓
huashan  : 华  山	murong   : 慕  容	xueshan  : 雪  山
xuedao   : 雪刀门	xiaoyao  : 逍遥林	lingjiu  : 灵鹫宫
guanwai  : 关  外	guiyun   : 归云庄	nanhai   : 南  海
wugong   : 武功镇	zhongnan : 终南山	yanziwu  : 燕子坞
heimuya  : 黑木崖	nanyang  : 南  阳	kunlun   : 昆仑山
yihua : 移花宫	beiyun  : 白云城 mojiao : 魔教

shaolin  : 少林派	baituo   : 白驼山	home     : 住  房
----------------------------------------------------------------------

HELP
	);

	return 1;
}
