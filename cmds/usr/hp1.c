
#include <ansi.h>

inherit F_CLEAN_UP;

string status_color(int current, int max);

int main(object me, string arg)
{
	object ob;
	mapping my;
	string sp;
	int hj;
	seteuid(getuid(me));
	ob = me;

	my = ob->query_entire_dbase();

	// if (userp(ob) && (! stringp(my["born"]) || ! my["born"]))
	// 	return notify_fail("还没有出生呐，察看什么？\n");


	if ( (int)me->query("vip/vip_time") >= time() ){
		//如果会员没过期

		if ( my["food"]<=50 || my["water"]<=50 ){
			me->set("food",ob->max_food_capacity());
			me->set("water",ob->max_water_capacity());
		}

	}


	if (my["max_jing"] < 1 || my["max_qi"] < 1)
		return notify_fail("无法察看" + ob->name(1) + "的状态。\n");

	//sp  = ZJHPTXT+sprintf("%s:100/100:%s",my["name"],"#000000");
	sp  = WZHPTXT;
	sp += ob->query("name");
	//sp += sprintf("经.%d:%d/%d:%s",my["combat_exp"],my["combat_exp"],F_LEVEL->level_max_exp(ob),"#FF00FF");
	sp +=  	sprintf(WZSEP"%s/%d/%d/%d/%s","气",my["qi"],my["eff_qi"],my["max_qi"],"#FF0000/#CF6262/#909090");
	sp += 	sprintf(WZSEP"%s/%d/%d/%d/%s","内",my["neili"],my["max_neili"],ob->query_current_neili_limit(),"#0471F9/#66A2ED/#909090");
	sp +=  	sprintf(WZSEP"%s/%d/%d/%d/%s","潜",(int)ob->query_potential(),(int)ob->query_potential(),(int)ob->query_max_potential(),"#00DEFF/#9DF1FE/#909090");

	//sp +=  	sprintf(WZSEP"%s/%d/%d/%d/%s","精",my["jing"],my["eff_jing"],my["max_jing"],"#00ff00/#d9d9d9/#909090");

	//sp += "║" + sprintf("潜.%d:%d/%d/%d:%s",(int)ob->query_potential(),(int)ob->query_potential(),(int)ob->query_potential(),(int)ob->query_max_potential(),"#006600");
	//sp += "║" + sprintf("怒.%d:%d/%d:%s",ob->query_craze(),ob->query_craze(),ob->query_max_craze(),"#990000");
	//sp += "║" + sprintf("精.%d:%d/%d/%d:%s",my["jingli"],my["jingli"],my["max_jingli"],ob->query_current_jingli_limit(),"#BB6600CC");
	//sp += "║" + sprintf("元.%d:%d/%d:%s",ob->query("yuanbao"),ob->query("yuanbao"),ob->query("yuanbao"),"#BBFF6600");
	// sp +=  sprintf(WZSEP"%s/%d/%d/%d/%s","元",to_int(ob->query("yuanbao")),to_int(ob->query("yuanbao")),to_int(ob->query("yuanbao")),"#00ff00/#d9d9d9/#909090");

	// hj = ob->query("balance");
	// hj = hj / 10000;

	// hj += ob->query("coffers")  * 1000;

	// sp += sprintf(WZSEP"%s/%d/%d/%d/%s","金",hj,hj,hj,"#00ff00/#d9d9d9/#909090");



	sp += NOR2;
	tell_object(me, sp);

	return 1;
}

int help(object me)
{
	write(@HELP
指令格式 : hp
	  hp <对象名称>		  (巫师专用)

这个指令可以显示你(你)或指定对象(含怪物)的精, 气, 神数值。

see also : score
HELP );
    return 1;
}
