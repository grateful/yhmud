// map.c

#include <ansi.h>

inherit F_CLEAN_UP;

int map_view(object me, string arg);
int map_rumor(object me, string arg);
int map_area(object player, string arg);
string  *areas = ({
		"平安镇-pingan",
		"扬州-yangzhou",
		"北京-beijing",
		"成都-chengdu",
		"长安-changan",
		"杭州-hangzhou",
		"苏州-suzhou",
		"襄阳-xiangyang",
		"福州-fuzhou",
		"佛山-foshan",
		"灵州-lingzhou",
		"衡阳-henyang",
		"嘉兴-jiaxing",
		"泉州-quanzhou",
		"大理-dali",
		"武当山-wudang",
		"全真教-quanzhen",
		"峨眉山-emei",
		"光明顶-mingjiao",
		"星宿海-xingxiu",
		"华山-huashan",
		"华山剑宗-huashanjianzong",
		"慕容-murong",
		"古墓-gumu",
		"雪山-xueshan",
		"雪刀门-xuedao",
		//"唐门-tangmen",
		"逍遥林-xiaoyao",
		"灵鹫宫-lingjiu",
		"移花宫-yihua",
		"胡家-hujia",
		"归云庄-guiyun",
		"南海-nanhai",
		"武功镇-wugong",
		"终南山-zhongnan",
		"燕子坞-yanziwu",
		"黑木崖-heimuya",
		"南阳-nanyang",
		"昆仑山-kunlun",
		"少林-shaolin",
		"神龙教-shenlong",
		"桃花岛-taohua",
		"白驼山-baituo",
		"白云城-baiyun",
		"魔教-mojiao",
		//"五毒教-wudu",
		"住房-home",
});





void create()
{
	seteuid(getuid());
}

int main(object me, string arg)
{
	string here, name;
	object env;
	string *mapped;
	int lvl;
	int exp, pot, score;
	int pot_limit;
	string msg;
	mixed prompt;
	if (! arg)
		return notify_fail("格式：map here | rumor | view | all | <地点> | <珍闻>。\n");

	if (arg == "rumor")
		return map_rumor(me, arg);

	if (arg == "world")
	{
		//write(WZMAPTXT+replace_string(color_filter(read_file("/help/mapall")),"\n",WZBR)+"\n");

		return 1;
	}
	//by fang Q184377367 支持客户端按钮显示
	if (arg == "area")
	{
		return map_area(me, arg);
	}
	//门派
	/*
	if (arg == "famy")
	{
		//return map_famy(me, arg);
	}*/


	if (arg != "here"){
		return map_view(me, arg);

	}




	// if (! me->query("out_family"))
	// {
	// 	write("你现在还没有从师傅那里领到地图册。\n");
	// 	return 1;
	// }

	// 查阅当前的环境
	env = environment(me);
	name = env->short();
	if (! stringp(here = env->query("outdoors")))
	{
		write("只有在户外才有必要绘制地图。\n");
		return 1;
	}

	if (! stringp(name) || clonep(env))
	{
		write("这里是一处神秘的地方，你无法判断它的方位。\n");
		return 1;
	}

	if (me->query("map_all"))
	{
		write("你已经获得了地图全集，没有必要再绘制地图了。\n");
		return 1;
	}

	if (me->is_busy())
	{
		write("你现在正忙，没有时间绘制地图。\n");
		return 1;
	}

	// 查阅已经绘制过的地图
	mapped = me->query("map/" + here);
	if (! arrayp(mapped)) mapped = ({ });
	if (member_array(name, mapped) != -1)
	{
		write("你已经绘制过这附近的地图了，没有必要再重复了。\n");
		return 1;
	}

	if (! MAP_D->been_known(here))
	{
		write("你觉得这里没什么好画的。\n");
		return 1;
	}

	if (prompt = env->query("no_map"))
	{
		if (stringp(prompt))
			write(prompt);
		else
			write("你看了半天，也没有弄清楚附近的地形。\n");
		return 1;
	}

	if (me->query("jing") < 50)
	{
		write("你的精神不佳，无法集中全神贯注的绘制地图。\n");
		return 1;
	}

	// 消耗精
	me->receive_damage("jing", 20 + random(30));

	if ((lvl = me->query_skill("drawing", 1)) < 30)
	{
		write("你在纸上涂抹了一会儿，连自己都看不出是什么东西。\n");
		return 1;
	}

	message("vision", me->name() + "抬头看了看四周，埋头仔仔细细的绘制着什么。\n", environment(me), ({ me }));
	tell_object(me, "你精心的绘制了" + name + "附近的地形。\n");

	mapped += ({ name });
	me->set("map/" + here, mapped);

	me->start_busy(1 + random(3));

	// 计算奖励
	if (lvl > 200)
		lvl = (lvl - 200) / 4 + 150;
	else
	if (lvl > 100)
		lvl = (lvl - 100) / 2 + 100;

	exp = 20 + random(20);
	pot = 5 + random((lvl - 20) / 3);
	score = random(2);
	pot_limit = me->query_potential_limit() - me->query("potential");
	if (pot_limit < 0)
		pot_limit = 0;
	if (pot >= pot_limit)
		pot = pot_limit;

	//  GIFT_D->bonus(me, ([ "exp" : exp,
	//                         "pot" : pot,
	//                         "score" : score,
	//                         "prompt":"通过体验",
	//  						]));


	return 1;
}


/*
查看区域地图
by fang
*/
int map_area(object player, string arg){

	// string strAreaname;
	// string *aryAreas,strWrContent;
	// mixed  *sort_c;


	// strWrContent="";
	// strWrContent+=ZJOBLONG"区域地图\n"ZJOBACTS2"$3,3,10,30#";

	// strWrContent+="附近地图:map view"+WZSEP;
	// strWrContent+="世界地图:map world"+WZSEP;
	// for(int i=0;i < sizeof(areas)-1;i++){
	// 	aryAreas = explode(areas[i],"-");
	// 	strWrContent+=aryAreas[0]+":fly "+aryAreas[1]+WZSEP;
	// }
	//  write(strWrContent+"\n");
	// return  1;
}


//门派地图
int map_famy(object player, string arg){

// 	string strAreaname;
// 	string *aryAreas,strWrContent;
// 	mixed  *sort_c;


// 	// strWrContent="";
// 	// strWrContent+=ZJOBLONG"区域地图\n"ZJOBACTS2"$3,3,10,30#";

// 	strWrContent+="附近地图:map view"+WZSEP;
// 	strWrContent+="世界地图:map world"+WZSEP;
// 	for(int i=0;i < sizeof(areas)-1;i++){
// 		aryAreas = explode(areas[i],"-");
// 		strWrContent+=aryAreas[0]+":fly "+aryAreas[1]+WZSEP;
// 	}


// 	 write(strWrContent+"\n");
// 	return  1;
}





// 查看已经绘制部分的地图
int map_view(object me, string arg)
{
	mapping mapped;
	mapping rumor;
	string outdoors;
	string *shorts;
	string result;
	string key;
	//MYGIFT_D->check_mygift(me, "newbie_mygift/look_adrenv");

	if (! me->query("out_family"))
	{

                write( WZMAPTXT"【*上下左右滑动展现更多】"WZSEP+MAP_D->marked_map(environment(me))+NOR2);
                return 1;
	}

	mapped = me->query("map");

	if (! me->query("map_all") && ! mapp(mapped))
	{

		   write( WZMAPTXT"【*上下左右滑动展现更多】"WZSEP+MAP_D->marked_map(environment(me))+NOR2);


		return 1;
	}

	if (me->is_busy())
	{
		write("你现在正忙，没法查看地图。\n");
		return 1;
	}

	message_vision("$N拿出一本东西，哗啦哗啦的翻开看了起来。\n", me);
	me->start_busy(1);

	// 察看是否阅读记载
	if (mapp(rumor = me->query("rumor")) && member_array(arg, keys(rumor)) != -1)
	{
		write("你翻到地图册的后面，仔细阅读有关『" + arg + "』的记载。\n" WHT + rumor[arg]->query_detail(arg) + NOR);
		return 1;
	}

	// 是否是察看本地地图？
	if (arg == "view" || me->query("map_all"))
	{
		if(!me->query("newplayer/look_map") && !me->query("newplayer/task/end"))
		{
			me->set("newplayer/look_map",1);
		}

			   write( WZMAPTXT"【*上下左右滑动展现更多】"WZSEP+MAP_D->marked_map(environment(me))+NOR2);

		me->start_busy(2);
		return 1;
	}

	// 判断是否是中文地图名字
	foreach (key in keys(mapped))
		if (MAP_D->query_map_short(key) == arg)
		{
			// 是中文名字，转换成英文ID
			arg = key;
			break;
		}

	// 查找这方面的地图
	if (arrayp(shorts = mapped[arg]))
	{
		result = MAP_D->query_maps(arg);
		foreach (key in shorts)
		{
			reset_eval_cost();
			if (! stringp(key))
				continue;
			result = MAP_D->mark_map(result, key);
		}
		result = replace_string(result, "@R", WHT);
		result = replace_string(result, "@N", NOR);
		me->start_more(MAP_D->query_map_short(arg) + "的地图信息：\n" + result);
		me->start_busy(2);
		return 1;
	}

	if (arg != "all")
	{
		write("你的地图册中并没有有关 " + arg + " 的信息啊！\n");
		return 1;
	}

	outdoors = environment(me)->query("outdoors");
	if (stringp(outdoors))
		result = "你现在身处" + MAP_D->query_map_short(outdoors) + "境内。\n";

	result = "目前你已经绘制了以下这些地方的地图：\n";
	foreach (key in keys(mapped))
		result += MAP_D->query_map_short(key) + "(" HIY + key + NOR ")\n";
	write(result);




	return 1;
}

int map_rumor(object me, string arg)
{
	mapping rumor;
	string msg;

	if (! mapp(rumor = me->query("rumor")))
	{
		write("你现在并没有记录任何传闻趣事。\n");
		return 1;
	}

	msg = "你目前记载了有关" + implode(keys(rumor), "、") + "的传闻。\n";
	msg = sort_string(msg, 60);
	write(msg);
	return 1;
}

int help(object me)
{
	write(@HELP
指令格式 : map here | rumor | all | view | <地点> | <珍闻>

如果你身上有了地图册，就可以使用这条命令绘制附近的地图。当然
你必须是在户外，并且具备有一定的绘画技巧才可以。绘制地图可以
增加你的经验、潜能并积累江湖阅历。

使用 map all 可以查看你现在已经绘制了哪些地方的地图， 而如果
指名了具体的地方，则可以查看该地点的地图绘制情况。

如果你在户外，则可以使用 map view 命令查看你所在的地点。

map rumor 可以查阅你目前记录的各地珍闻，使用 map <珍闻> 则可
以查看具体内容。
HELP );
    return 1;
}
